#ifndef __SCROLLTEMPLATE_H_
#define __SCROLLTEMPLATE_H_

//! Includes
// The following define skips compilation of ScrollEd (map editor) for now
#define __NO_SCROLLED__
#include "Scroll.h"

//! OrxScroll class
class ScrollTemplate : public Scroll<OrxScroll>
{
public:

private:
	//! Initialize the program
	virtual orxSTATUS Init();
	//! Callback called every frame
	virtual orxSTATUS Run();
	//! Exit the program
	virtual void      Exit();
};

#endif // __SCROLLTEMPLATE_H_