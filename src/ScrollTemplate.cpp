//! Includes
// The following define/undef is done only once in the project. It should be
// done before including the interface of the class deriving from
// Scroll (as follows).
#define __SCROLL_IMPL__
#include "ScrollTemplate.h"
#undef __SCROLL_IMPL__

orxSTATUS ScrollTemplate::Init()
{
	orxSTATUS result = orxSTATUS_SUCCESS;

	return result;
}

orxSTATUS ScrollTemplate::Run()
{
	orxSTATUS result = orxSTATUS_SUCCESS;

	return result;
}

void ScrollTemplate::Exit()
{
}

int main(int argc, char **argv)
{
	// Executes game
	ScrollTemplate::GetInstance().Execute(argc, argv);

	// Done!
	return EXIT_SUCCESS;
}