//! Includes
// The following define/undef is done only once in the project. It should be
// done before including the interface of the class deriving from
// Scroll (as follows).
#define __SCROLL_IMPL__
#include "MyScrollObject.h"
#undef __SCROLL_IMPL__

orxSTATUS MyScrollObject::Init()
{
	orxSTATUS result = orxSTATUS_SUCCESS;

	return result;
}

orxSTATUS MyScrollObject::Run()
{
	orxSTATUS result = orxSTATUS_SUCCESS;

	return result;
}

void MyScrollObject::Exit()
{
}

int main(int argc, char **argv)
{
	// Executes game
	MyScrollObject::GetInstance().Execute(argc, argv);

	// Done!
	return EXIT_SUCCESS;
}